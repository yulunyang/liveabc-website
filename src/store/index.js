import 'babel-polyfill'
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '../router'
import createPersistedState from 'vuex-persistedstate'
import VueCookie from 'vue-cookie'
Vue.use(Vuex)

/* eslint-disable */
const state = {
  auth: {
    authorized: false,
    userRole: '',
    user: null
  },
  alert: {
    isAlert: false,
    isHead: true,
    alertMsg: '尚未登入',
    type: 'alert'
  },
  isAddCart: false,
  isAddWish: false,
  isLoading: false,

  // 置頂通知
  isIndexTopNotice: false,
  isIndexTopMainBody: true,

  // 首頁banner
  isIndexAlert: true,

  routeRecord: 'index',
  errorMessage: '',
  cart: {
    list: null,
    totalPrice: 0,
    num: 0
  },
  cartNum: 0,
  order: {
    orderNum: null
  },
  apiUrl: process.env.NODE_ENV === 'production' ? 'https://api.liveabc.com' : 'https://apitest.liveabc.com',
  nodeEnv: process.env.NODE_ENV
}

const getters = {
  user: state => state.auth.user,
  account: state => state.auth.user.name,
  authorized: state => state.auth.authorized,
  userRole: state => state.auth.userRole,
  isLoading: state => state.isLoading,
  isAlert: state => state.alert.isAlert,
  isIndexAlert: state => state.isIndexAlert,
  alertMsg: state => state.alert.alertMsg,
  cartNum: state => state.cart.num,
  cartList: state => state.cart.list,
  totalPrice: state => state.cart.totalPrice,
  errorMessage: state => state.errorMessage,
  order: state => state.order,
  routeRecord: state => state.routeRecord,
  nodeEnv: state => state.nodeEnv,
  isIndexTopNotice: state => state.isIndexTopNotice,
  isIndexTopMainBody: state => state.isIndexTopMainBody
}

const actions = {
  isIndexTopNotice: (context, data) => {
    context.commit('ISINDEXTOPNOTICE', data)
  },
  isIndexTopMainBody: (context, data) => {
    context.commit('ISINDEXTOPBODY', data)
  },
  updateIndexAlert (context, data) {
    context.commit('INDEXALERT', data)
  },

  isLoading: (context, data) => {
    context.commit('ISLOADING', data)
  },
  isAddingCart: (context, data) => {
    context.commit('ISADDCART', data)
  },
  isAddingWish: (context, data) => {
    context.commit('ISADDWISH', data)
  },
  updateAlert (context, data) {
    context.commit('ALERTING', data)
  },

  refreshToken: async () => {
    let { data } = await axios.get('/user/token/refresh')
    // localStorage.setItem('authToken', data.access_token)
    VueCookie.set('token', data.access_token)
    window.location.reload()
  },

  register: async ({ dispatch }, user) => {
    await axios.post('/register', {
      'name': user.name,
      'password': user.password,
      'account': user.account,
      'code': user.code,
      'isAgreeReceiveDM': user.isAgreeReceiveDM
    })
    .then(() => {
      dispatch('login', { account: user.account, password: user.password })
      router.push({ name: 'jump' })
    })
    .catch(() => {
      dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '驗證碼錯誤或帳號已存在', type: 'alert' })
    })
  },

  login: async ({ commit, dispatch }, user) => {
    await axios.post('/login', {
      'email': user.account,
      'password': user.password
    })
    .then(response => {
      commit('LOGIN', response.data)
      // localStorage.setItem('authToken', response.data.access_token)
      dispatch('getProfile')
      VueCookie.set('token', response.data.access_token)
    })
    .catch(error => {
      if (error.response.status === 401) {
        dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '帳號密碼錯誤', type: 'alert' })
      }
    })
  },

  loginFB: async ({ commit, dispatch }, accessVal) => {
    await axios.post('/facebook_login', {
      'access_token': accessVal.accessToken.authResponse.accessToken
    })
    .then(response => {
      commit('LOGIN', response.data)
      // localStorage.setItem('authToken', response.data.access_token)
      VueCookie.set('token', response.data.access_token)
      dispatch('getProfile')
    })
    .catch((error) => {
      console.log(error)
      dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: 'E-mail帳號未建立', type: 'alert' })
    })
  },

  useTokenLogin: async({ commit, dispatch }, token) => {
    VueCookie.set('token', token)
    await dispatch('getProfile')

    let loginToken = { access_token: token, expires_in: 28800, token_type: 'bearer' }
    commit('LOGIN', loginToken)
  },

  getProfile: async ({ commit, dispatch }) => {
    let { data } = await axios.get('/user/me')
    if (data && data.error) {
      dispatch('logout')
    } else {
      commit('UPDATEPROFILE', data)
    }
  },

  logout: async ({ commit }) => {
    await axios.post('/logout')
      .then(() => {
        commit('LOGOUT')
      }).catch(() => {
        commit('LOGOUT')
        // router.push({ name: 'index' })
      })
  },

  // 更新購物車項目
  updateCart: ({ commit }) => {
    var productsNum = 0
    var cartTotal = 0
    if (localStorage.getItem('products')) {
      let products = JSON.parse(localStorage.getItem('products'))
      for (let item of products) {
        productsNum += item.quantity
        cartTotal += item.fixed_price
      }
      let data = [productsNum, products, cartTotal]
      commit('UPDATECART', data)
    } else {
      let data = [0, [], 0]
      commit('UPDATECART', data)
    }
  },

  // 紀錄route
  routeRecord: (context, data) => {
    context.commit('ROUTERECORD', data)
  }
}

const mutations = {
  ISINDEXTOPNOTICE (state, data) {
    state.isIndexTopNotice = data
  },
  ISINDEXTOPBODY (state, data) {
    state.isIndexTopMainBody = data
  },
  INDEXALERT (state, data) {
    state.isIndexAlert = data
  },
  ISLOADING (state, data) {
    state.isLoading = data
  },
  ROUTERECORD (state, data) {
    state.routeRecord = data.routeName
  },
  ISADDCART (state, data) {
    state.isAddCart = data
  },
  ISADDWISH (state, data) {
    state.isAddWish = data
  },
  ALERTING (state, data) {
    state.alert.isAlert = data.isAlert
    state.alert.isHead = data.isHead
    state.alert.alertMsg = data.alertMsg
    state.alert.type = data.type
  },
  LOGIN (state) {
    state.auth.authorized = true
    // state.auth.user = data
  },

  UPDATEPROFILE (state, data) {
    state.auth.userRole = data.role
    state.auth.user = {
      'id': data.id,
      'fb_id': data.fb_id,
      'email': data.email,
      'mobile': data.mobile,
      'name': data.name,
      'address': data.contact_info,
      'birth': data.birth,
      'email_verified': data.email_verified_at ? true : false,
      'mobile_verified': data.mobile_verified_at ? true : false,
      'isAgreeReceiveDM': data.is_agree_receive_dm ? true : false,
      'invoice_donation': data.invoice_donation
    }
  },

  // fb login
  LOGINFB (state, data) {
    state.auth.authorized = true
    state.auth.user = data.authResponse
  },

  UPDATEPROFILEFB (state, data) {
    state.auth.userRole = 'FBuser'
    state.auth.user = {
      'id': data.id,
      'email': data.email,
      'name': data.name
    }
  },

  LOGOUT (state) {
    state.auth.authorized = false
    state.auth.user = {}
    state.auth.userRole = ''
    // localStorage.removeItem('authToken')
    VueCookie.delete('token')
  },

  setErrorMessage (state, message) {
    state.errorMessage = message
  },

  UPDATECART (state, data) {
    state.cart.num = data[0]
    state.cart.list = data[1]
    state.cart.totalPrice = data[2]
  }
}

const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  modules: {
  },
  strict: debug,
  plugins: [createPersistedState({
    paths: [
      'auth'
    ]
  })]
})
