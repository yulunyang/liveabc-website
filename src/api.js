import axios from 'axios'
import store from './store/index'
import router from './router'
import VueCookie from 'vue-cookie'
axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? 'https://api.liveabc.com/v1' : 'https://apitest.liveabc.com/v1'
axios.defaults.timeout = 40000

let exclude = ['/login', '/check', '/barcode_check', '/product', '/products', '/register']

axios.interceptors.request.use(request => {
  // const token = localStorage.getItem('authToken')
  const token = VueCookie.get('token')
  if (token && exclude.indexOf(request.url) === -1) {
    request.headers.common['Authorization'] = `Bearer ${token}`
  }
  axios.defaults.withCredentials = false

  return request
})

axios.interceptors.response.use(response => {
  if (response.data.error && response.data.error === 'TOKEN_EXPIRED') {
    store.dispatch('refreshToken')
  }
  if (response.data.error && response.data.error === 'TOKEN_INVALID') {
    store.dispatch('logout')
    router.push({ name: 'login' })
    store.dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '登入資訊未更新', type: 'alert' })
  }
  return response
}, error => {
  console.log(error)
  if (error.response) {
    store.commit('setErrorMessage', error.response)
    let { status } = error.response
    if (status === 429) {
      store.dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '系統忙碌中，請稍後再試', type: 'alert' })
    }
  } else {
    store.commit('setErrorMessage', error.message)
  }

  var originalRequest = error.config
  if (error.code === 'ECONNABORTED' && error.message.indexOf('timeout') !== -1 && !originalRequest._retry) {
    originalRequest._retry = true
    return axios.request(originalRequest)
  }

  return Promise.reject(error)
})
