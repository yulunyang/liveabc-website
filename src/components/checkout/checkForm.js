import jump from 'jump.js'
import axios from 'axios'
var checkForm = {
  data () {
    return {
      validation: {
        // 訂購人
        name: true,
        phone: true,
        phoneRex: true,
        tel: true,
        address: true,
        email: true,
        pwd: true,
        pwdCheck: true,
        pwdEqal: true,
        pwdRex: true,
        emailRex: true,
        isEmailVerify: true,
        isCityDistrict: true,

        // 收件人
        recipient_name: true,
        recipient_phone: true,
        recipient_tel: true,
        recipient_address: true,
        recipient_rexPhone: true,
        recipient_notmailAddress: true,
        isCODandOutland: false,
        recipient_cityDistrict: true,

        // 發票
        // 統一編號
        taxNum: true,
        // 統一編號抬頭
        taxName: true,
        // 中講寄送地址
        invoice_cityDistrict: true,
        invoice_address: true,
        // 手機條碼
        invoice_carrier: true,
        // 手機條碼格式
        rex_invoice_carrier: true,
        // 受贈機關或團體
        isInputLoveCode: true,
        // 手機條碼是否存在
        isBarCode: true,
        // 捐贈代碼
        invoice_donation: true
      }
    }
  },
  methods: {
    handleInputNumber (e) {
      this.shoppingInfo.delivery.taxNum = e.target.value.replace(/[^\d]/g, '')
    },
    // 檢測單一欄位
    checField (field) {
      let self = this
      switch (field) {
        // 訂購資訊
        case 'userName':
          self.shoppingInfo.user.name ? self.validation.name = true : self.validation.name = false
          break
        case 'userPhone':
          self.shoppingInfo.user.phone ? self.validation.phone = true : self.validation.phone = false
          self.rexMobile(self.shoppingInfo.user.phone) ? self.validation.phoneRex = true : self.validation.phoneRex = false
          break
        case 'userTel':
          // self.shoppingInfo.user.tel ? self.validation.tel = true : self.validation.tel = false
          break
        case 'userAddress':
          self.shoppingInfo.user.address ? self.validation.address = true : self.validation.address = false
          break
        // 未登入設定帳號密碼
        case 'userMail':
          self.shoppingInfo.user.email ? self.validation.email = true : self.validation.email = false
          self.rexEmail(self.shoppingInfo.user.email) ? self.validation.emailRex = true : self.validation.emailRex = false
          break
        case 'password':
          self.shoppingInfo.user.pwd ? self.validation.pwd = true : self.validation.pwd = false
          self.rexPassword(self.shoppingInfo.user.pwd) ? self.validation.pwdRex = true : self.validation.pwdRex = false
          break
        case 'passwordCheck':
          self.shoppingInfo.user.pwdCheck ? self.validation.pwdCheck = true : self.validation.pwdCheck = false
          self.checkPwdEqual(self.shoppingInfo.user.pwd, self.shoppingInfo.user.pwdCheck) ? self.validation.pwdEqal = true : self.validation.pwdEqal = false
          break
        // 收件資訊
        case 'recipientName':
          self.shoppingInfo.recipient.name ? self.validation.recipient_name = true : self.validation.recipient_name = false
          break
        case 'recipientPhone':
          self.shoppingInfo.recipient.phone ? self.validation.recipient_phone = true : self.validation.recipient_phone = false
          self.rexMobile(self.shoppingInfo.recipient.phone) ? self.validation.recipient_rexPhone = true : self.validation.recipient_rexPhone = false
          break
        case 'recipientTel':
          // self.shoppingInfo.recipient.tel ? self.validation.recipient_tel = true : self.validation.recipient_tel = false
          break
        case 'recipientAddress':
          self.shoppingInfo.recipient.address ? self.validation.recipient_address = true : self.validation.recipient_address = false
          break
        // 統編
        case 'taxNum':
          self.rexTaxNum(self.shoppingInfo.delivery.taxNum) ? self.validation.taxNum = true : self.validation.taxNum = false
          break
        case 'taxName':
          self.shoppingInfo.delivery.taxName ? self.validation.taxName = true : self.validation.taxName = false
          break
        // 自然人憑證
        case 'NaturalPersonCertify':
          self.shoppingInfo.delivery.invoice_carrier ? self.validation.invoice_carrier = true : self.validation.invoice_carrier = false
          self.rexCitizenDigitalCertify(self.shoppingInfo.delivery.invoice_carrier) ? self.validation.rex_invoice_carrier = true : self.validation.rex_invoice_carrier = false
          break
        // 手機條碼
        case 'MobilePhoneBarcode':
          self.shoppingInfo.delivery.invoice_carrier ? self.validation.invoice_carrier = true : self.validation.invoice_carrier = false
          self.rexMobileBarcode(self.shoppingInfo.delivery.invoice_carrier) ? self.validation.rex_invoice_carrier = true : self.validation.rex_invoice_carrier = false
          break
        case 'invoice_address':
          self.shoppingInfo.delivery.invoice_address ? self.validation.invoice_address = true : self.validation.invoice_address = false
          break
      }
    },

    // 檢測表格是否填寫
    async checkformDevelope () {
      /* eslint-disable no-unused-expressions */
      let self = this

      // 訂購資訊
      // self.shoppingInfo.user.tel ? self.validation.tel = true : self.validation.tel = false
      self.shoppingInfo.user.name ? self.validation.name = true : self.validation.name = false
      self.shoppingInfo.user.phone ? self.validation.phone = true : self.validation.phone = false
      self.shoppingInfo.user.address ? self.validation.address = true : self.validation.address = false
      self.rexMobile(self.shoppingInfo.user.phone) ? self.validation.phoneRex = true : self.validation.phoneRex = false
      self.shoppingInfo.user.city && self.shoppingInfo.user.district ? self.validation.isCityDistrict = true : self.validation.isCityDistrict = false

      // 收件資訊
      self.shoppingInfo.recipient.name ? self.validation.recipient_name = true : self.validation.recipient_name = false
      self.shoppingInfo.recipient.phone ? self.validation.recipient_phone = true : self.validation.recipient_phone = false
      self.rexMobile(self.shoppingInfo.recipient.phone) ? self.validation.recipient_rexPhone = true : self.validation.recipient_rexPhone = false
      self.shoppingInfo.recipient.address ? self.validation.recipient_address = true : self.validation.recipient_address = false
      self.shoppingInfo.recipient.city && self.shoppingInfo.recipient.district ? self.validation.recipient_cityDistrict = true : self.validation.recipient_cityDistrict = false
      // self.shoppingInfo.recipient.tel ? self.validation.recipient_tel = true : self.validation.recipient_tel = false

      // 發票資訊
      if (self.shoppingInfo.delivery.invoice_type === '31') {
        // 會員載具
        self.validation.isBarCode = true
        if (self.shoppingInfo.delivery.eGUIType === 0 && !self.shoppingInfo.delivery.invoice_giving) {
          self.shoppingInfo.delivery.invoice_city && self.shoppingInfo.delivery.invoice_district ? self.validation.invoice_cityDistrict = true : self.validation.invoice_cityDistrict = false
          self.shoppingInfo.delivery.invoice_address ? self.validation.invoice_address = true : self.validation.invoice_address = false
        } else {
          self.validation.invoice_address = true
        }
      } else if (self.shoppingInfo.delivery.invoice_type === '32') {
        // 自然人憑證條碼
        self.validation.isBarCode = true
        self.shoppingInfo.delivery.invoice_carrier ? self.validation.invoice_carrier = true : self.validation.invoice_carrier = false
        self.rexCitizenDigitalCertify(self.shoppingInfo.delivery.invoice_carrier) ? self.validation.rex_invoice_carrier = true : self.validation.rex_invoice_carrier = false
      } else if (self.shoppingInfo.delivery.invoice_type === '33' || self.shoppingInfo.delivery.invoice_type === '330') {
        // 手機條碼
        if (self.shoppingInfo.delivery.invoice_carrier.length === 8) {
          self.validation.invoice_carrier = true
          if (self.rexMobileBarcode(self.shoppingInfo.delivery.invoice_carrier)) {
            self.validation.rex_invoice_carrier = true
            await self.checkBarCode()
          } else {
            self.validation.rex_invoice_carrier = false
          }
        } else {
          self.validation.invoice_carrier = false
        }
      } else if (self.shoppingInfo.delivery.invoice_type === '0') {
        self.validation.invoice_carrier = true
        self.validation.rex_invoice_carrier = true
        self.validation.isBarCode = true
      }
      if (self.shoppingInfo.delivery.eGUIType === 0 && self.shoppingInfo.delivery.invoice_giving) {
        // 捐贈
        self.shoppingInfo.delivery.invoice_donation ? self.validation.invoice_donation = true : self.validation.invoice_donation = false
      } else {
        self.validation.invoice_donation = true
      }

      if (self.shoppingInfo.delivery.eGUIType === 2) {
        // 統編
        self.rexTaxNum(self.shoppingInfo.delivery.taxNum) ? self.validation.taxNum = true : self.validation.taxNum = false
        self.shoppingInfo.delivery.taxName ? self.validation.taxName = true : self.validation.taxName = false
      }
      // 未登入資訊檢核
      if (!self.authorized) {
        self.shoppingInfo.user.email ? self.validation.email = true : self.validation.email = false
        self.shoppingInfo.user.pwd ? self.validation.pwd = true : self.validation.pwd = false
        self.shoppingInfo.user.pwdCheck ? self.validation.pwdCheck = true : self.validation.pwdCheck = false
        self.checkPwdEqual(self.shoppingInfo.user.pwd, self.shoppingInfo.user.pwdCheck) ? self.validation.pwdEqal = true : self.validation.pwdEqal = false
        self.rexPassword(self.shoppingInfo.user.pwd) ? self.validation.pwdRex = true : self.validation.pwdRex = false
        self.rexEmail(self.shoppingInfo.user.email) ? self.validation.emailRex = true : self.validation.emailRex = false
      }

      // 貨到付款地址不能有郵政/信箱字串
      if (self.paymentInfo.payment === 'COD') {
        self.shoppingInfo.recipient.address.indexOf('郵政') === -1 ? self.validation.recipient_notmailAddress = true : self.validation.recipient_notmailAddress = false
        self.shoppingInfo.recipient.address.indexOf('信箱') === -1 ? self.validation.recipient_notmailAddress = true : self.validation.recipient_notmailAddress = false
        self.isOutIsland ? self.validation.isCODandOutland = false : self.validation.isCODandOutland = true
      } else {
        self.validation.isCODandOutland = true
      }

      let count = 0

      for (let index in Object.values(self.validation)) {
        if (Object.values(self.validation)[index] === false) {
          count++
        }
      }

      if (count === 0) {
        return true
      } else {
        return false
      }
    },
    checkoutContract () {
      let self = this
      let count = 0
      if (self.agreements && self.agreements.length > 0) {
        for (let item of self.agreements) {
          if (!item.isAgree) {
            count++
          }
        }

        if (count > 0) {
          return false
        } else {
          return true
        }
      } else {
        return true
      }
    },
    jumpFunc () {
      let self = this
      if (!self.validation.name || !self.validation.phone || !self.validation.phoneRex || !self.validation.tel || !self.validation.address || !self.validation.email || !self.validation.pwd || !self.validation.pwdCheck || !self.validation.pwdEqal || !self.validation.pwdRex || !self.validation.emailRex || !self.validation.isEmailVerify) {
        jump(self.$refs.form, {
          duration: 800,
          offset: -100
        })
      } else if (!self.validation.invoice_carrier || !self.validation.rex_invoice_carrier || !self.validation.invoice_address || !self.validation.recipient_name || !self.validation.recipient_phone || !self.validation.recipient_tel || !self.validation.recipient_address || !self.validation.recipient_rexPhone || !self.validation.recipient_notmailAddress || !self.validation.isInputLoveCode || !self.validation.taxNum || !self.validation.taxName) {
        jump(self.$refs.recipientForm, {
          duration: 800,
          offset: -100
        })
      }
    },
    jumpFuncDevelope () {
      let self = this
      if (!self.validation.name || !self.validation.phone || !self.validation.phoneRex || !self.validation.tel || !self.validation.address || !self.validation.email || !self.validation.pwd || !self.validation.pwdCheck || !self.validation.pwdEqal || !self.validation.pwdRex || !self.validation.emailRex || !self.validation.isEmailVerify || !self.validation.isCityDistrict) {
        jump(self.$refs.orderer, {
          duration: 800,
          offset: -100
        })
      } else if (!self.validation.recipient_name || !self.validation.recipient_phone || !self.validation.recipient_tel || !self.validation.recipient_address || !self.validation.recipient_rexPhone || !self.validation.recipient_notmailAddress || !self.validation.recipient_cityDistrict) {
        jump(self.$refs.recipientForm, {
          duration: 800,
          offset: -100
        })
      } else if (!self.validation.invoice_carrier || !self.validation.rex_invoice_carrier || !self.validation.invoice_address || !self.validation.isInputLoveCode || !self.validation.taxNum || !self.validation.taxName || !self.validation.invoice_cityDistrict) {
        jump(self.$refs.eGUI, {
          duration: 800,
          offset: -100
        })
      }
    },
    async checkBarCode () {
      let self = this
      // '/89DUDKQ'

      await axios.post('/barcode_check', {
        barCode: self.shoppingInfo.delivery.invoice_carrier
      })
        .then((response) => {
          response.data.exist ? self.validation.isBarCode = true : self.validation.isBarCode = false
        })
    }
  }
}

export default checkForm
