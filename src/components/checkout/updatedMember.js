import axios from 'axios'
var updateMember = {
  methods: {
    // 更新會員資料
    updateMember () {
      let self = this
      if (self.isUpdatedMember && self.authorized) {
        let data = {
          name: self.shoppingInfo.user.name,
          tel: self.shoppingInfo.user.tel
        }

        axios.patch('/user/me', data)
          .then(() => {
            self.$store.dispatch('getProfile')
          })
          .catch((error) => {
            console.log(error)
          })

        // 地址
        let addData = {
          city: self.shoppingInfo.user.city,
          district: self.shoppingInfo.user.districts,
          address: self.shoppingInfo.user.address,
          zip_code: self.shoppingInfo.delivery.eGUIType === 0 && self.shoppingInfo.delivery.invoice_giving && self.shoppingInfo.delivery.invoice_donation ? self.shoppingInfo.delivery.invoice_donation : self.shoppingInfo.user.zip_code,
          mobile: self.shoppingInfo.user.phone
        }

        // eslint-disable-next-line no-unused-expressions
        self.shoppingInfo.user.tel ? addData.tel = self.shoppingInfo.user.tel : null

        axios.patch('/user/recipients/' + self.user.address.id, addData)
          .then(() => {
            self.$store.dispatch('getProfile')
          })
          .catch((error) => {
            console.log(error)
          })
      }
    }
  }
}

export default updateMember
