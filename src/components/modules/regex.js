var Rex = {
  methods: {
    rexEmail (val) {
      // eslint-disable-next-line no-useless-escape
      let pattern = new RegExp(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/)
      if (val && val.match(pattern)) {
        return true
      } else {
        return false
      }
    },
    rexMobile (val) {
      // eslint-disable-next-line no-useless-escape
      let pattern = new RegExp(/^09\d{8}$/)
      if (val && val.match(pattern)) {
        return true
      } else {
        return false
      }
    },
    checkPwdEqual (val, newVal) {
      if (val && newVal && val === newVal) {
        return true
      } else {
        return false
      }
    },
    rexPassword (val) {
      // eslint-disable-next-line no-useless-escape
      let pattern = new RegExp(/^(?=.*\d)(?=.*[a-zA-Z]).{8,15}$/)
      if (val && val.match(pattern)) {
        return true
      } else {
        return false
      }
    },
    rexNumber (val) {
      // eslint-disable-next-line no-useless-escape
      let pattern = new RegExp(/^[0-9]*$/)
      if (val && val.match(pattern)) {
        return true
      } else {
        return false
      }
    },
    rexTaxNum (val) {
      // 統編
      // eslint-disable-next-line no-useless-escape
      let pattern = new RegExp(/^[0-9]{8}$/)
      if (val && val.match(pattern)) {
        return true
      } else {
        return false
      }
    },
    rexCitizenDigitalCertify (val) {
      // 自然人憑證條碼
      // eslint-disable-next-line no-useless-escape
      let pattern = new RegExp(/^[A-Z]{2}\d{14}/)
      if (val && val.match(pattern)) {
        return true
      } else {
        return false
      }
    },
    rexMobileBarcode (val) {
      // 手機條碼
      // eslint-disable-next-line no-useless-escape
      let pattern = new RegExp(/^\/\d|[A-Z]|\.|\+|-{0,7}/)
      if (val && val.match(pattern) && val.length === 8) {
        return true
      } else {
        return false
      }
    }
  }
}

export default Rex
