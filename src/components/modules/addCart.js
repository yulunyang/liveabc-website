// import axios from 'axios'
import './advertising'
var Cart = {
  methods: {
    getItemUrl (type) {
      switch (type) {
        case 'BOOK':
          return '/book/detail/'
        case 'PACKAGE':
          return '/package/detail/'
        case 'ITEM':
          return '/package/detail/'
        case 'MAGAZINE':
          return '/magazine/detail/'
        case 'SUBSCRIPTION':
          return '/magazine/detail/'
      }
    },
    async popupAddCart () {
      let self = this
      if (self.$store.state.isAddCart) {
        await self.$store.dispatch('isAddingCart', false)
        self.$store.dispatch('isAddingCart', true)
      } else {
        self.$store.dispatch('isAddingCart', true)
      }
    },

    // 加入購物車
    async addShoppingList (item, gifts) {
      /* eslint-disable */
      let self = this
      let newItem = {
        id: item.id,
        quantity: 1,
        name: item.name,
        cover: item.cover,
        type: item.type,
        link_name: item.link_name ? item.link_name : '',
        link_id: item.link_id,
        begin_at: item.begin_at ? item.begin_at : '',
        end_at: item.end_at ? item.end_at : '',
        // selected_gifts: item.selected_gifts || gifts ? item.selected_gifts : '',
        isRegisteredMail: item.isRegisteredMail ? item.isRegisteredMail : false,
        is_digital: item.is_digital ? item.is_digital : false,
        expire_time: Date.now() + (1000 * 60 * 60 * 24 * 7)
      }
      item.selected_gifts ? newItem.selected_gifts = item.selected_gifts : null
      gifts ? newItem.selected_gifts = gifts : null

      if (item.type === 'SUBSCRIPTION' && item.link_id) {
        newItem.link = self.getItemUrl(item.type) + item.link_id
      } else {
        newItem.link =  self.getItemUrl(item.type) + item.id
      }

      var saveCart = (val) => {
        if (window.localStorage) {
          localStorage.setItem('products', JSON.stringify(val))
          self.$store.dispatch('updateCart')
        }
      }

      // 限制數位產品數量為20
      let productsitems = JSON.parse(localStorage.getItem('products'))
      let digitalCount = 0
      if (productsitems && productsitems.length > 0) {
        for (let child of productsitems) {
          if (child.is_digital) {
            digitalCount += child.quantity
          }
        }
      }

      if (digitalCount >= 20 && item.is_digital) {
        self.$store.dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '已達電腦互動學習軟體購買上限（20筆）', type: 'alert' })
      } else {
        if (localStorage.getItem('products')) {
          // 如果已經有localStorage
          let products = JSON.parse(localStorage.getItem('products'))
          let idList = products.map(item => Object.values(item)[0])
          let index = idList.indexOf(item.id)
          if (idList.indexOf(item.id) !== -1) {
            // 有重複項目
            if (item.type === 'SUBSCRIPTION') {
              // 雜誌同期數限制一本
              self.$store.dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '購物車已有同一本雜誌(期數)囉!，欲改數量可至購物車', type: 'alert' })
            } else if (item.type !== 'SUBSCRIPTION' && products[index].quantity < item.in_stock && products[index].quantity < item.in_stock) {
              // 其他商品數量+1
              if (products[index].selected_gifts) {
                self.$store.dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '購物車已有同一本商品囉!，欲改數量可至購物車', type: 'alert' })
              } else {
                products[index].quantity ++
                saveCart(products)
                self.popupAddCart()
              }
            }
          } else {
            // 沒有重複項目
            products.push(newItem)
            saveCart(products)
            self.popupAddCart()
          }
        } else {
          // 第一次加入購物車
          let array = []
          array.push(newItem)
          saveCart(array)
          self.popupAddCart()
        }
      }
      // 廣告商code
      self.addCartCode({ 'productID': item.id, 'unit': '1', 'price': item.pricing })
    },
    // 更新購物車
    async updatedShoppingList (item) {
      /* eslint-disable */
      let self = this
      let newItem = {
        id: item.id,
        quantity: 1,
        name: item.name,
        cover: item.cover,
        type: item.type,
        link_name: item.link_name ? item.link_name : '',
        link_id: item.link_id,
        begin_at: item.begin_at ? item.begin_at : '',
        end_at: item.end_at ? item.end_at : '',
        selected_gifts: item.selected_gifts ? item.selected_gifts : '',
        isRegisteredMail: item.isRegisteredMail ? item.isRegisteredMail : false,
        is_digital: item.is_digital ? item.is_digital : false,
        expire_time: Date.now() + (1000 * 60 * 60 * 24 * 7)
      }

      if (item.type === 'SUBSCRIPTION' && item.link_id) {
        newItem.link = self.getItemUrl(item.type) + item.link_id
      } else {
        newItem.link =  self.getItemUrl(item.type) + item.id
      }

      var saveCart = (val) => {
        if (window.localStorage) {
          localStorage.setItem('products', JSON.stringify(val))
          self.$store.dispatch('updateCart')
        }
      }

      // 限制數位產品數量為20
      let productsitems = JSON.parse(localStorage.getItem('products'))
      let digitalCount = 0
      if (productsitems && productsitems.length > 0) {
        for (let child of productsitems) {
          if (child.is_digital) {
            digitalCount += child.quantity
          }
        }
      }

      if (digitalCount >= 20 && item.is_digital) {
        self.$store.dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '已達電腦互動學習軟體購買上限（20筆）', type: 'alert' })
      } else {
        if (localStorage.getItem('products')) {
          // 如果已經有localStorage
          let products = JSON.parse(localStorage.getItem('products'))
          let idList = products.map(item => Object.values(item)[0])
          let index = idList.indexOf(item.id)
          if (idList.indexOf(item.id) !== -1) {
            // 有重複項目
            if (item.type === 'SUBSCRIPTION') {
              // 雜誌同期數限制一本
              self.$store.dispatch('updateAlert', { isAlert: true, isHead: true, alertMsg: '購物車已有同一本雜誌(期數)囉!，欲改數量可至購物車', type: 'alert' })
            } else if (item.type !== 'SUBSCRIPTION' && products[index].quantity < item.in_stock && products[index].quantity < item.in_stock) {
              // 其他商品數量+1
              products[index].quantity ++
              saveCart(products)
            }
          } else {
            // 沒有重複項目
            products.push(newItem)
            saveCart(products)
          }
        } else {
          // 第一次加入購物車
          let array = []
          array.push(newItem)
          saveCart(array)
        }
      }
    },
    // 變更購物車商品數量
    updateQuantity (id, num) {
      let self = this
      let products = JSON.parse(localStorage.getItem('products'))
      let idList = products.map(item => Object.values(item)[0])
      let index = idList.indexOf(id)
      products[index].quantity = num

      localStorage.setItem('products', JSON.stringify(products))
      self.$store.dispatch('updateCart')
    },

    // 刪除購物車
    removeShoppingList () {
      let self = this
      localStorage.removeItem('products')
      self.$store.dispatch('updateCart')
    },

    // 刪除購物車項目
    removeShoppingItem (id) {
      let self = this
      let products = JSON.parse(localStorage.getItem('products'))

      if (products &&　products.length > 1) {
        let items = products.filter(c => c.id !== id)
        localStorage.setItem('products', JSON.stringify(items))
      } else {
        localStorage.removeItem('products')
      }

      self.$store.dispatch('updateCart')
    },

    // 刪除過期商品
    removeExpireItem () {
      let self = this
      if (JSON.parse(localStorage.getItem('products'))) {
        let checkItem = JSON.parse(localStorage.getItem('products'))

        for (let item of checkItem) {
          if (Date.now() > item.expire_time && item.expire_time || !item.expire_time) {
            self.removeShoppingItem(item.id)
            self.$emit('removeShopItem', 'refreshCart')
          }
        }
      }
    }
  }
}

export default Cart
