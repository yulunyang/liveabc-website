/* eslint-disable no-unused-expressions */
var advertising = {
  methods: {
    loginCode (email) {
      // 事件 3-1：使用者登入
      var appierRtUserEmail = email
      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_login',
          'ids': [{ 'idtype': 'email_sha256', 'content': appierRtUserEmail, 'needSHA256': true }],
          'action_id': 'UserLogin_dddc',
          'track_id': '0b36ee568675270',
          'opts': { 'uu': appierRtUserEmail }
        }
      )
    },
    searchCode (keyword, data) {
      // 事件 4：使用者站內關鍵字查詢
      var appierRtSearch = []
      var appierRtProductIDList = []
      let keywords = keyword.split(/\s+/)

      for (let i = 0; i < keywords.length; i++) {
        appierRtSearch.push(keywords[i])
      }
      if (data.length > 10) {
        for (let i = 0; i < 10; i++) {
          appierRtProductIDList.push(data[i].id.toString())
        }
      } else {
        for (let i = 0; i < data.length; i++) {
          appierRtProductIDList.push(data[i].id.toString())
        }
      }

      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_listpage',
          'keywords': appierRtSearch,
          'productIDList': appierRtProductIDList
        }
      )
    },
    likeCatgeogryCode (category, data) {
      // 事件 5：關注類別頁面
      var appierRtCategory = category
      var appierRtProductIDList = []
      if (data.length > 10) {
        for (let i = 0; i < 10; i++) {
          appierRtProductIDList.push(data[i].id.toString())
        }
      } else {
        for (let i = 0; i < data.length; i++) {
          appierRtProductIDList.push(data[i].id.toString())
        }
      }
      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_listpage', 'categoryIDs': appierRtCategory, 'productIDList': appierRtProductIDList }
      )
    },
    likeProductCode (id, price) {
      // 事件 6：關注商品頁面
      var appierRtProduct = [{ 'productID': id.toString(), 'price': price.toString() }]

      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_product', 'itemList': appierRtProduct, 'action_id': 'ViewProduct_022c', 'track_id': '0b36ee568675270' }
      )
    },
    addWishCode (wishData) {
      // 事件 7：點擊加入願望清單按鈕
      var appierRtAddToWishlist = []
      appierRtAddToWishlist.push(wishData)

      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_addwishlist', 'itemList': appierRtAddToWishlist, 'action_id': 'AddToWishList_13a7', 'track_id': '0b36ee568675270' }
      )
    },
    viewWishCode (data) {
      // 事件 8：瀏覽願望清單頁(頁面)
      var appierRtWishList = []

      for (let i = 0; i < data.length; i++) {
        appierRtWishList.push({ 'productID': data[i].id.toString(), 'unit': '1', 'price': data[i].pricing.toString() })
      }

      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_wishlist', 'itemList': appierRtWishList, 'action_id': 'ViewWishList_f6d0', 'track_id': '0b36ee568675270' }
      )
    },
    addCartCode (data) {
      // 事件 9：點擊加入購物車(按鈕)
      var appierRtAddToCart = []
      appierRtAddToCart.push(data)

      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_addcart', 'itemList': appierRtAddToCart, 'action_id': 'AddToCart_4c41', 'track_id': '0b36ee568675270' }
      )
    },
    viewCartCode (data) {
      // 事件 10：瀏覽購物車頁(頁面)
      var appierRtCartList = []
      var appierRtPrice = data.total

      for (let i = 0; i < data.items.length; i++) {
        appierRtCartList.push({ 'productID': data.items[i].id, 'unit': data.items[i].quantity, 'price': data.items[i].pricing.toString() })
      }

      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { 't': 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_cart',
          'itemList': appierRtCartList,
          'totalvalue': appierRtPrice,
          'action_id': 'ViewCart_50b2',
          'track_id': '0b36ee568675270' }
      )
    },
    checkoutFinishCode (orderId, data, calcAmount) {
      // 事件 11：購物完成
      var appierRtorderId = orderId.toString()

      var appierRtItemList = []
      for (let i = 0; i < data.length; i++) {
        appierRtItemList.push({ 'productID': data[i].id.toString(), 'unit': data[i].quantity.toString(), 'price': data[i].pricing.toString() })
      }

      var appierRtPrice = calcAmount.toString()
      var appierRtCurrency = 'TWD'

      window.appier_q = window.appier_q || []
      window.appier_q.push(
        { t: 'register', 'content': { 'id': 'f28a', 'site': 'store.liveabc.com' } },
        { 't': 'type_purchase',
          'itemList': appierRtItemList,
          'totalvalue': appierRtPrice,
          'currency': appierRtCurrency,
          'action_id': 'Purchase_ea52',
          'track_id': '0b36ee568675270',
          'opts': { 'uu': appierRtorderId,
            'action_param1': JSON.stringify(appierRtItemList),
            'action_param2': appierRtorderId,
            'total_revenue': appierRtPrice,
            'currency': appierRtCurrency
          }
        }
      )
    }
  }
}

export default advertising
