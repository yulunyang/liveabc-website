import 'babel-polyfill'
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import VueMeta from 'vue-meta'
import router from './router'
import store from './store/index'
// import VueGtm from 'vue-gtm'
// import VueAnalytics from 'vue-analytics'

import moment from 'moment'
import './assets/application.js'
import './assets/Ad.js'
import './api'
import './css/all.scss'
import './css/sweat_alert.scss'
// import './common/font/font.css'

import VueFacebookPixel from 'vue-analytics-facebook-pixel'
import VueProgressiveImage from 'vue-progressive-image'
import VueClipboard from 'vue-clipboard2'

import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'
import AOS from 'aos'
import 'aos/dist/aos.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { far } from '@fortawesome/free-regular-svg-icons'

import { faShoppingCart, faHome, faUserFriends, faPlay, faSearchPlus, faPlus, faMinus, faCircle, faCheckCircle, faCircleNotch, faShoppingBag, faExclamationCircle, faVolumeUp, faCheck, faSearch, faUser, faUserSlash, faAngleDown, faHeart, faAngleLeft, faAngleRight, faAngleDoubleRight, faAngleDoubleLeft, faChevronLeft, faChevronRight, faTimes, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import VueCookie from 'vue-cookie'
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload, {
  preLoad: 1.4,
  listenEvents: ['scroll'],
  attempt: 1
})
Vue.use(VueCookie)
library.add(far, faShoppingCart, faHome, faUserFriends, faPlay, faSearchPlus, faPlus, faMinus, faCheckCircle, faCircle, faHeart, faAngleDoubleLeft, faAngleDoubleRight, faAngleLeft, faAngleRight, faChevronLeft, faChevronRight, faTimes, faTimesCircle, faAngleDown, faUser, faUserSlash, faSearch, faCheck, faVolumeUp, faExclamationCircle, faShoppingBag, faCircleNotch)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.component('v-select', vSelect)
Vue.use(VueAwesomeSwiper)
Vue.use(Vuex)
Vue.use(VueSweetalert2)

// Vue.use(VueGtm, {
//   id: ['GTM-55Z744R'],
//   enabled: process.env.NODE_ENV === 'production',
//   defer: false,
//   debug: true,
//   loadScript: true,
//   vueRouter: router,
//   ignoredViews: ['index']
// })
Vue.use(VueMeta, {
  hasMetaInfo: true,
  keyName: 'metaInfo',
  tagIDKeyName: 'vmid',
  attribute: 'data-vue-meta',
  ssrAttribute: 'data-vue-meta-server-rendered',
  ssrAppId: 'ssr',
  contentKeyName: 'content',
  refreshOnceOnNavigation: true
})

Vue.use(VueProgressiveImage, {
  cache: true,
  blur: 30
})
Vue.use(VueFacebookPixel)
Vue.analytics.fbq.init('188825209155719', {
})

Vue.prototype.moment = moment

Vue.config.productionTip = false

// Vue.use(VueAnalytics, {
//   id: 'UA-78537321-7',
//   autoTracking: {
//     pageviewOnLoad: false
//   },
//   router,
//   debug: {
//     enabled: false
//   }
// })

Vue.use(VueClipboard)
Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})
new Vue({
  created () {
    AOS.init()
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app')
