import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/Index.vue'
import DigitalLearn from './pages/onlinePlatform/DigitalLearn.vue'
// 主題
import Topic from './views/Topic.vue'
import ArticleList from './pages/article/ArticleList.vue'
import ArticleContent from './pages/article/ArticleContent.vue'

// 點讀館
import ReadingPen from './views/ReadingPen.vue'
import PenLangingPage from './pages/livepen/LandinPpage.vue'
import Download from './pages/livepen/Download.vue'
import IntroductionNew from './pages/livepen/IntroPenRenew.vue'

// 產品頁
import ListView from './views/ListView.vue'
import ProductList from './pages/product/List.vue'
import ProductDetail from './pages/product/Detail.vue'
import Sale from './pages/product/Sale.vue'
import Magazine from './pages/magazine/CurrentMagazine'
import MagazineDetail from './pages/magazine/MagazineDetail'

// 會員相關資料
import User from './views/User.vue'
import OrderList from './pages/member/OrderList.vue'
import OrderDetail from './pages/member/OrderDetail.vue'
import Wish from './pages/member/WishList.vue'
import Account from './pages/member/Account.vue'
import ShippingAddress from './pages/member/ShippingAddress.vue'
import Manage from './pages/member/Manage.vue'
import ForgotPwd from './pages/ForgotPwd.vue'
import Login from './pages/Login.vue'
import Register from './pages/Register.vue'
import Jump from './pages/Jump.vue'
import FBLogin from './pages/FBLogin.vue'
import ResetPassword from './pages/company/ResetPassword.vue'
import SubscriberNumbering from './pages/member/SubscriberNumbering.vue'
import Class from './pages/member/Class.vue'
// import MagazineMaturity from './pages/member/MagazineMaturity.vue'
// import MagazineChangeAddress from './pages/member/MagazineChangeAddress.vue'
// import MagazineTransform from './pages/member/MagazineTransform.vue'
// import MagazineSendBack from './pages/member/MagazineSendBack.vue'
// import ProductWarranty from './pages/member/ProductWarranty.vue'

// 結帳
import Checkout from './pages/Checkout.vue'

// 公司頁面
import Company from './views/Company.vue'
import About from './pages/company/About.vue'
import Privacy from './pages/company/Privacy.vue'
import Policy from './pages/company/Policy.vue'
import Service from './pages/company/Service.vue'
import Contact from './pages/company/Contact.vue'
import Connect from './pages/company/Connect.vue'
import ContractRevise from './pages/company/ContractRevise.vue'
import ContractLTTC from './pages/company/ContractLTTC.vue'
import ContractLiveTalk from './pages/company/ContractLiveTalk.vue'
import MagTransform from './pages/company/MagTransform.vue'

// 網頁相關頁面
import SearchResult from './pages/SearchResult.vue'
import Header from './components/modules/headerSection.vue'
import Footer from './components/modules/footerSection.vue'

import Unsubscribe from './pages/company/Unsubscribe.vue'
import EmailVerification from './pages/company/emailVerification.vue'
// 每日一句
import DailySentence from './pages/company/DailySentence.vue'
// LiveTalk
import LiveTalk from './pages/onlinePlatform/LiveTalk.vue'

// 活動頁
import Event from './views/Event.vue'
import Samsung from './pages/events/Samsung.vue'
import BookFair2020 from './pages/events/BookFair2020.vue'
import SinglesDay2020 from './pages/events/SinglesDay2020.vue'
import Double12in2020 from './pages/events/Double12_2020.vue'
import ACD18 from './pages/events/ACD18.vue'
import event202108 from './pages/events/event202108.vue'
import Mzfestival from './pages/events/Mzfestival.vue'
import FUNHIW from './pages/events/FUNHIW.vue'
import DoubleOct2021 from './pages/events/Double10_2021.vue'
// 其他
// import Downtime from './pages/Downtime.vue'

// 圖書館
// import Library from './views/Library.vue'
// import MagazineHouse from './pages/library/MagazineHouse.vue'
// import ClassList from './pages/library/ClassList.vue'
// import Lesson from './pages/library/Lesson.vue'
Vue.use(Router)

let routes = [
  // 停機公告頁
  // {
  //   path: '/*',
  //   name: 'Downtime',
  //   components: {
  //     default: Downtime,
  //     nav: Header,
  //     footer: Footer
  //   }
  // },
  // 首頁
  {
    path: '/',
    name: 'index',
    components: {
      default: Index,
      nav: Header,
      footer: Footer
    }
  },
  // 搜尋結果
  {
    path: '/search/',
    name: 'SearchResult',
    components: {
      default: SearchResult,
      nav: Header,
      footer: Footer
    }
  },
  {
    path: '/forgotPwd',
    name: 'forgotPwd',
    components: {
      default: ForgotPwd,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '忘記密碼',
      gtm: 'login'
    }
  },
  // 登入
  {
    path: '/login',
    name: 'login',
    components: {
      default: Login,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '會員登入',
      gtm: 'login'
    }
  },
  // 註冊
  {
    path: '/register',
    name: 'register',
    components: {
      default: Register,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '會員註冊',
      gtm: 'register'
    }
  },
  // 註冊
  {
    path: '/register_success',
    name: 'jump',
    components: {
      default: Jump,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '會員註冊',
      gtm: 'register'
    }
  },
  {
    path: '/login_FB',
    name: 'FBlogin',
    components: {
      default: FBLogin,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '會員登入',
      gtm: 'FBlogin'
    }
  },
  // 購物車
  {
    path: '/checkout/:step?',
    name: 'checkout',
    components: {
      default: Checkout,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '購物車',
      gtm: 'checkout'
    }
  },
  // 收藏
  {
    path: '/favorite',
    name: 'favorite',
    components: {
      default: Wish,
      nav: Header,
      footer: Footer
    },
    meta: { requiresAuth: true }
  },
  // 會員相關資料
  {
    path: '/user',
    components: {
      default: User,
      nav: Header,
      footer: Footer
    },
    meta: { requiresAuth: true },
    children: [
      {
        path: '',
        name: 'manage',
        component: Manage
      },
      {
        path: 'order',
        name: 'order',
        component: OrderList
      },
      {
        path: 'orderdetail/:id',
        name: 'orderdetail',
        component: OrderDetail
      },
      {
        path: 'account',
        name: 'account',
        component: Account
      },
      {
        path: 'common-address',
        name: 'commonAddress',
        component: ShippingAddress
      },
      {
        path: 'my-class',
        name: 'class',
        component: Class
      }
    ]
  },
  // 公司相關頁面
  {
    path: '/company',
    name: 'company',
    components: {
      default: Company,
      nav: Header,
      footer: Footer
    },
    children: [
      {
        path: 'about',
        name: 'about',
        component: About,
        meta: {
          requiresAuth: false,
          title: '品牌故事'
        }
      },
      {
        path: 'contract-revise',
        name: 'contract-revise',
        component: ContractRevise,
        meta: {
          requiresAuth: false,
          title: '批改網服務契約'
        }
      },
      {
        path: 'contract-LTTC',
        name: 'contract-lttc',
        component: ContractLTTC,
        meta: {
          requiresAuth: false,
          title: 'LTTC英文檢定練習服務契約'
        }
      },
      {
        path: 'contract-LiveTalk',
        name: 'contract-livetalk',
        component: ContractLiveTalk,
        meta: {
          requiresAuth: false,
          title: 'LiveTalk網際網路服務契約'
        }
      },
      {
        path: 'policy',
        name: 'policy',
        component: Policy,
        meta: {
          requiresAuth: false,
          title: '服務條款'
        }
      },
      {
        path: 'privacy',
        name: 'privacy',
        component: Privacy,
        meta: {
          requiresAuth: false,
          title: '隱私權政策'
        }
      },
      {
        path: 'contact',
        name: 'contact',
        component: Contact,
        meta: {
          requiresAuth: false,
          title: '業務合作洽談'
        }
      },
      {
        path: 'connect',
        name: 'connect',
        component: Connect,
        meta: {
          requiresAuth: false,
          title: '客服信箱'
        }
      },
      // QA
      {
        path: 'service/:id?',
        name: 'service',
        component: Service,
        meta: {
          requiresAuth: false,
          title: '常見問題'
        }
      },
      // 雜誌轉換
      {
        path: 'magazineTransform',
        name: 'magazineSendBack',
        component: MagTransform
      },
      // {
      //   path: 'productWarranty',
      //   name: 'productWarranty',
      //   // component: ProductWarranty
      //   component: Connect
      // },
      // {
      //   path: 'magazineMaturity',
      //   name: 'magazineMaturity',
      //   // component: MagazineMaturity
      //   component: Connect
      // },
      {
        path: 'subscriberNumbering',
        name: 'subscriberNumbering',
        component: SubscriberNumbering
      }
      // {
      //   path: 'magazineChangeAddress',
      //   name: 'magazineChangeAddress',
      //   // component: MagazineChangeAddress
      //   component: Connect
      // },
      // {
      //   path: 'magazineTransform',
      //   name: 'magazineTransform',
      //   // component: MagazineTransform
      //   component: Connect
      // },
      // {
      //   path: 'magazineSendBack',
      //   name: 'magazineSendBack',
      //   // component: MagazineSendBack
      //   component: Connect
      // }
    ]
  },
  // 商品相關頁
  {
    path: '/products',
    name: 'ListView',
    components: {
      default: ListView,
      nav: Header,
      footer: Footer
    },
    children: [
      {
        // 雜誌列表
        path: 'newest-magazine',
        name: 'magazine',
        component: Magazine
      },
      {
        // 雜誌各刊
        path: 'newest-magazine/:class',
        name: 'magazineIntro',
        component: MagazineDetail
      },
      {
        // 商品列表
        path: ':category',
        name: 'productsList',
        component: ProductList
      },
      {
        // 活動
        path: ':category?/sale/:tag?',
        name: 'sale',
        component: Sale
      },
      {
        // 詳細頁
        path: '/:category?/detail/:id?',
        name: 'detail',
        component: ProductDetail
      },
      {
        // 預覽
        path: '/:category?/preview/:id?',
        name: 'previewMagazine',
        component: ProductDetail
      }
    ]
  },
  // 主題專欄
  {
    path: '/topic',
    name: 'topic',
    components: {
      default: Topic,
      nav: Header,
      footer: Footer
    },
    children: [
      {
        path: 'list/:topicCategory?',
        name: 'topicList',
        component: ArticleList
      },
      {
        path: 'article/:id?',
        name: 'topicDetail',
        component: ArticleContent
      }
    ]
  },
  // 點讀筆
  {
    path: '/smartPen',
    name: 'pen',
    components: {
      default: ReadingPen,
      nav: Header,
      footer: Footer
    },
    children: [
      {
        path: 'index',
        name: 'readpen',
        component: PenLangingPage,
        meta: {
          requiresAuth: false
        }
      },
      {
        path: 'intro',
        name: 'intro',
        component: IntroductionNew,
        meta: {
          requiresAuth: false
        }
      },
      {
        path: 'download',
        name: 'download',
        component: Download,
        meta: {
          requiresAuth: false
        }
      }
    ]
  },
  {
    path: '/web/smartPen/intro',
    name: 'intro_new',
    components: {
      default: IntroductionNew,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false
    }
  },
  // 數位學習
  {
    path: '/learning',
    name: 'learning',
    components: {
      default: DigitalLearn,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false
    }
  },
  // 電子報
  {
    path: '/unsubscribe',
    name: 'unsubscribe',
    components: {
      default: Unsubscribe,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '取消訂閱電子報'
    }
  },
  // 修改密碼
  {
    path: '/reset_password',
    name: 'ResetPassword',
    components: {
      default: ResetPassword,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '修改密碼'
    }
  },
  // 信箱驗證
  {
    path: '/validate/subscribe/:email/:token/:action',
    name: 'validate',
    components: {
      default: EmailVerification,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '信箱驗證'
    }
  },
  // 每日一句
  {
    path: '/daily-sentence',
    name: 'dailySentence',
    components: {
      default: DailySentence,
      nav: Header,
      footer: Footer
    },
    meta: {
      requiresAuth: false,
      title: '每日一句'
    }
  },
  {
    path: '/liveTalk',
    name: 'liveTalk',
    component: LiveTalk,
    meta: {
      requiresAuth: false,
      title: 'LiveTalk線上真人口說教室'
    }
  },
  // Events
  {
    path: '/event',
    name: 'event',
    component: Event,
    children: [
      {
        path: 'SamsungP610',
        name: 'samsungP610',
        component: Samsung
      },
      {
        path: 'bookFair2020',
        name: 'bookFair2020',
        component: BookFair2020
      },
      {
        path: 'SinglesDay2020',
        name: 'SinglesDay2020',
        component: SinglesDay2020
      },
      {
        path: '2020Double12',
        name: '2020Double12',
        component: Double12in2020
      },
      {
        path: 'ACD18',
        name: 'ACD18',
        component: ACD18
      },
      {
        path: 'event202108',
        name: 'event202108',
        component: event202108
      },
      {
        path: 'mzfestival',
        name: 'mzfestival',
        component: Mzfestival
      },
      {
        path: 'FUNHIW',
        name: 'FUNHIW',
        component: FUNHIW
      },
      {
        path: 'Double10_2021',
        name: 'double10_2021',
        component: DoubleOct2021
      }
    ]
  }
  // {
  //   path: '/knowledge',
  //   name: 'knowledge',
  //   components: {
  //     default: Library,
  //     nav: Header
  //     // footer: Footer
  //   },
  //   children: [
  //     {
  //       path: '',
  //       name: 'magHouse',
  //       component: MagazineHouse
  //     },
  //     {
  //       path: 'magHouse/:categoryId',
  //       name: 'classList',
  //       component: ClassList
  //     },
  //     {
  //       path: 'magHouse/theme/:categoryId',
  //       name: 'themeList',
  //       component: ClassList
  //     },
  //     {
  //       path: 'lesson/:type/:id',
  //       name: 'lesson',
  //       component: Lesson
  //     }
  //   ]
  // }
]

const router = new Router({
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    window.scrollTo({ 'top': 0 })
    // return { x: 0, y: 0 }
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 500)
    })
  }
})

export default router
