module.exports = {
  presets: [
    ['@vue/babel-preset-app', {
      'useBuiltIns': 'entry',
      'polyfills': [
        'es6.promise',
        'es6.symbol',
        './src/main.js'
      ]
    }]
  ]
}
